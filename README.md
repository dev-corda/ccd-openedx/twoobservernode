# Observer node Cordapp

To run the Cordapp follow the below steps:

## 1. Build the project

Windows: `gradlew deployNodes`  

Ubuntu: `./gradlew deployNodes`  

## 2. Run the network

Windows: `build\nodes\runnodes.bat`  

Ubuntu: `build/nodes/runnodes`  

## 3. Sample Transaction

Select KBA node console  

`start LoanRequestFlow requestValue: 1000000, bank: Bank1`

## 4. To view the vault

`run vaultQuery contractStateType: com.template.states.LoanRequestState`